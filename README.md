# OpenBSD Router Guide

This is the public Codeberg repository for the [OpenBSD router guide](https://unixsheikh.com/tutorials/openbsd-router-guide/).

## How to Contribute

 * Clone and edit
 * Submit pull request for consideration

You can also just use [email](https://www.unixsheikh.com/contact.html) :) I prefer a [diff](https://en.wikipedia.org/wiki/Diff), but any contribution is greatly appreciated.

## License

OpenBSD Router Guide is released under the [BSD-3-Clause license](https://opensource.org/licenses/BSD-3-Clause).
